package com.tylermercer.timeboxer.tasks.addtask

import com.tylermercer.timeboxer.repositories.tasks.TaskRepository

private val durations: IntArray = IntArray(11, {i -> i*5 + 10})

class AddTaskPresenter(
        private var view: AddTaskContract.View,
        private var repository: TaskRepository
) : AddTaskContract.Presenter {
    private var durationMinutes: Int

    init {
        durationMinutes = durations[2]
    }

    override fun onViewCreated() {
        view.setProgressBarSize(durations.size-1)
        view.setSliderPosition(2)
        view.setTimeSelection(durationMinutes)
    }

    override fun onTimeSliderChanged(pos: Int) {
        durationMinutes = durations[pos]
        view.setTimeSelection(durationMinutes)
    }

    override fun onSubmit(task: String) {
        val durationSeconds = durationMinutes*60
        repository.addTask(task.trim(), durationSeconds)
        view.onTaskAdded(task, durationSeconds)
    }

}