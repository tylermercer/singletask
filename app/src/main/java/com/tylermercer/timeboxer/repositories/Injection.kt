package com.tylermercer.timeboxer.repositories

import com.tylermercer.timeboxer.utils.AppExecutors
import com.tylermercer.timeboxer.localstorage.SingletaskDatabase
import com.tylermercer.timeboxer.repositories.tasks.DefaultTaskRepository
import com.tylermercer.timeboxer.repositories.tasks.TaskRepository

object Injection {

    private lateinit var repository: TaskRepository

    fun provideTaskRepository(): TaskRepository {
        synchronized(this) {
            if (!::repository.isInitialized) {
                //Can be changed to a mock repository for testing
                repository = DefaultTaskRepository(AppExecutors(), provideDatabase())
            }
        }
        return repository
    }

    private fun provideDatabase(): SingletaskDatabase {
        //Can be changed to a mock database for testing
        return SingletaskDatabase()
    }
}