package com.tylermercer.timeboxer.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tylermercer.timeboxer.R

class FirstLaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_launch)
    }
}
