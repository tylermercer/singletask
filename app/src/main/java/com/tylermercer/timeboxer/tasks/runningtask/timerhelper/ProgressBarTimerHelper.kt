package com.tylermercer.timeboxer.tasks.runningtask.timerhelper

import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.Transformation
import android.widget.ProgressBar
import kotlin.math.max
import kotlin.math.min

private const val SCALE_FACTOR = 100

class ProgressBarTimerHelper(
        private val maxTimeSeconds: Int,
        private val progressBar: ProgressBar,
        private val listener: TimerHelper.OnTotalTimeElapsedListener?
) : TimerHelper {

    init {
        progressBar.max = maxTimeSeconds * SCALE_FACTOR
        progressBar.progress = 0
    }

    override var currentTimeRemaining: Int
        get() { return (progressBar.max - progressBar.progress)/SCALE_FACTOR }
        set(value) { progressBar.progress = max(0, min(progressBar.max, progressBar.max - value*SCALE_FACTOR))}

    override var currentTimeElapsed: Int
        get() { return (progressBar.progress / SCALE_FACTOR)}
        set(value) {progressBar.progress = max(0, min(progressBar.max, value* SCALE_FACTOR))}

    override val isRunning: Boolean
        get() { return running }

    private var animation: Animation? = null
    private var running = false

    override fun start() {

        val startProgress = progressBar.progress

        animation = object: Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                super.applyTransformation(interpolatedTime, t)
                if (isRunning) {
                    val value: Float = (startProgress + (progressBar.max - startProgress) * interpolatedTime)
                    progressBar.progress = (value).toInt()
                }
            }
        }

        animation?.apply {
            duration = currentTimeRemaining*1000L
            interpolator = LinearInterpolator()
        }

        if (listener != null) {
            animation?.setAnimationListener(object: Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {}

                override fun onAnimationEnd(p0: Animation?) {
                    listener.onTotalTimeElapsed()
                    animation?.setAnimationListener(null)
                }

                override fun onAnimationStart(p0: Animation?) {}
            })
        }
        running = true
        progressBar.startAnimation(animation)
    }


    override fun stop() {
        pause()
        progressBar.progress = 0
    }

    override fun pause() {
        animation?.setAnimationListener(null)
        running = false
        animation?.cancel()
        animation = null
        progressBar.max = maxTimeSeconds * SCALE_FACTOR
    }
}