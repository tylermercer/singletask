package com.tylermercer.timeboxer.repositories.tasks

import com.tylermercer.timeboxer.utils.AppExecutors
import com.tylermercer.timeboxer.localstorage.SingletaskDatabase
import com.tylermercer.timeboxer.repositories.entities.tasks.MutableRunningTask
import com.tylermercer.timeboxer.repositories.entities.tasks.MutableTask

/**
 * The default implementation of [link TaskRepository]. Acts as an aggregate root for the task
 * entities (including tasks, tags, etc), and contains the business logic pertaining to those
 * entities.
 *
 * @invariant: The current state of the repository is saved to the Database (if it exists)
 */
class DefaultTaskRepository (val appExecutors: AppExecutors, val database: SingletaskDatabase) : TaskRepository {

    private var runningTask: MutableRunningTask? = null
    private var queuedTask: MutableTask? = null

    /**
     * Gets the currently running task, or starts the next task in the queue if there isn't a running one
     */
    override fun getRunningTask(listener: TaskRepository.OnRunningTaskRetrievedListener) {
        if (runningTask != null) {
            listener.onRunningTaskRetrieved(runningTask!!)
        }
        else if (queuedTask != null) {
            //Create running task from existing queued task
            runningTask = MutableRunningTask(queuedTask!!)
            listener.onRunningTaskRetrieved(runningTask!!)
            appExecutors.diskIO.execute {
                database.startTransaction()
                database.queuedTasksDao.removeTask(runningTask!!.id)
                database.runningTaskDao.setRunningTask(runningTask!!)
                database.endTransaction(true)
            }
        }
        else {
            //Get running task from disk
            appExecutors.diskIO.execute {
                appExecutors.mainThread.execute {
                    //TODO: flesh out this logic -- probably need a fourth case for when the task is queued on disk and not running
                    database.startTransaction()
                    runningTask = database.runningTaskDao.getRunningTask()
                    if (runningTask == null) {
                        runningTask = MutableRunningTask(database.queuedTasksDao.getQueuedTask())
                    }
                    database.endTransaction(true)
                    listener.onRunningTaskRetrieved(runningTask!!);
                }
            }
        }
    }

    override fun addTask(description: String, durationSeconds: Int) {
        queuedTask = MutableTask(description, durationSeconds)
        //TODO: save to persistence
    }
}