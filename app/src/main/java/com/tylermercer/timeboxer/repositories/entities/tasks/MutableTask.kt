package com.tylermercer.timeboxer.repositories.entities.tasks

import android.util.Log
import java.util.*

class MutableTask(override val description: String, override val durationSeconds: Int) : Task {
    override val id: String = "$description-${durationSeconds}seconds-${UUID.randomUUID()}" //TODO: Make more Kotlin-idiomatic
    init {
        Log.i("MutableTask", "Task created! ID: $id")
    }
}