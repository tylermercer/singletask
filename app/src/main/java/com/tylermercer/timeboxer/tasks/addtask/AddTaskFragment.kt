package com.tylermercer.timeboxer.tasks.addtask

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.tylermercer.timeboxer.R
import com.tylermercer.timeboxer.repositories.Injection
import kotlinx.android.synthetic.main.fragment_add_task.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AddTaskFragment.OnTaskAddListener] interface
 * to handle interaction events.
 * Use the [AddTaskFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class AddTaskFragment : Fragment(), AddTaskContract.View {

    override fun setProgressBarSize(size: Int) {
        seekbar_duration.max = size
    }

    override fun setSliderPosition(pos: Int) {
        seekbar_duration.progress = pos
    }

    /**
     * Changes the displayed text to indicate the given duration
     */
    override fun setTimeSelection(durationMinutes: Int) {
        textview_duration.text = String.format(Locale.ENGLISH, getString(R.string.textview_num_minutes), durationMinutes)
    }

    private lateinit var presenter: AddTaskContract.Presenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = AddTaskPresenter(this, Injection.provideTaskRepository())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.onViewCreated()

        seekbar_duration.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekbar: SeekBar?, pos: Int, fromUser: Boolean) {
                presenter.onTimeSliderChanged(pos)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })

        btn_start_task.setOnClickListener { _ ->
            presenter.onSubmit(et_task_description.text.toString())
        }

        btn_start_task.isEnabled = false

        et_task_description.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btn_start_task.isEnabled = start + count != 0
            }

            override fun afterTextChanged(s: Editable?) {}
        })
    }

    private var listener: OnTaskAddListener? = null

    override fun onTaskAdded(description: String, durationSeconds: Int) {
        listener?.onTaskAdded(description, durationSeconds)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTaskAddListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnTaskAddListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnTaskAddListener {
        fun onTaskAdded(description: String, durationSeconds: Int)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment AddTaskFragment.
         */
        // TODO: Determine if parameters are needed here
        @JvmStatic
        fun newInstance() =
                AddTaskFragment()
    }
}
