package com.tylermercer.timeboxer.repositories.entities.tasks

/**
 * Immutable interface for Tasks
 */
interface Task {
    val description: String
    val durationSeconds: Int
    val id: String
}