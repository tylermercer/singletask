package com.tylermercer.timeboxer.tasks.runningtask.timerhelper

import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.widget.TextView
import kotlin.math.abs
import kotlin.math.min

class TextViewTimerHelper(
        private val maxTimeSeconds: Int,
        private val textView: TextView,
        private val listener: TimerHelper.OnTotalTimeElapsedListener?
) : TimerHelper {

    override var currentTimeRemaining: Int = maxTimeSeconds
        set(value) {
            field = min(maxTimeSeconds, value)
            updateTextView()
        }

    override var currentTimeElapsed: Int
        get() = maxTimeSeconds - currentTimeRemaining
        set(value) {
            currentTimeRemaining = min(maxTimeSeconds, maxTimeSeconds - value)
        }

    override val isRunning: Boolean
        get() = running

    private var running: Boolean = false
    private lateinit var timer: Runnable
    private var timerHandler: Handler = Handler()

    init {
        updateTextView()
    }

    override fun start() {
        val startTime: Long = SystemClock.uptimeMillis()

        timer = object : Runnable {
            var nextRun: Long = startTime + 1000
            override fun run() {
                if (!running) return

                nextRun += 1000
                --currentTimeRemaining
                if (currentTimeRemaining == 0) {
                    listener?.onTotalTimeElapsed()
                }
                timerHandler.postAtTime(this, nextRun)
            }
        }
        running = true
        timerHandler.postAtTime(timer, startTime + 1000)
    }

    private fun updateTextView() {
        Log.d("TextViewTimerHelper", "updateTextView called with $currentTimeRemaining left");
        textView.text = String.format("%s%d:%02d",
                if (currentTimeRemaining < 0) "-" else "",
                abs(currentTimeRemaining) / 60,
                abs(currentTimeRemaining) % 60)
    }

    override fun stop() {
        timerHandler.removeCallbacksAndMessages(null)
        running = false
        currentTimeRemaining = maxTimeSeconds
    }

    override fun pause() {
        timerHandler.removeCallbacksAndMessages(null)
        running = false
    }
}
