package com.tylermercer.timeboxer.tasks.addtask

import com.tylermercer.timeboxer.BasePresenter

interface AddTaskContract {
    interface View {
        fun setTimeSelection(durationMinutes: Int)
        fun onTaskAdded(description: String, durationSeconds: Int)
        fun setSliderPosition(pos: Int)
        fun setProgressBarSize(size: Int)
    }
    interface Presenter : BasePresenter {
        fun onTimeSliderChanged(pos: Int)
        fun onSubmit(task: String)
    }
}