package com.tylermercer.timeboxer.localstorage

import android.arch.persistence.room.Database
import com.tylermercer.timeboxer.repositories.entities.tasks.MutableTask

@Database(entities = arrayOf(MutableTask::class), version = 1)
abstract class SingletaskDatabase {
    abstract val runningTaskDao: RunningTaskDao
    abstract val queuedTasksDao: QueuedTasksDao
}
