package com.tylermercer.timeboxer.tasks.runningtask

import com.tylermercer.timeboxer.BasePresenter

interface RunningTaskContract {
    interface View {
        fun initTimer(description: String, duration: Int, elapsed: Int, running: Boolean)
    }
    interface Presenter : BasePresenter {
        fun onPauseTimer(timeElapsed: Int)
        fun onStartTimer()
        fun onStopTimer()
    }
}