package com.tylermercer.timeboxer.repositories.entities.tasks

/**
 * Encapsulates Task details as well as information necessary for a Running Task, such as
 * notification times
 */
class MutableRunningTask(private var wrapped: MutableTask): RunningTask {
    override val isPaused: Boolean
        get() = paused
    override val elapsedTimeSeconds: Int
        get() = elapsedTime
    override val description: String
        get() = wrapped.description
    override val durationSeconds: Int
        get() = wrapped.durationSeconds
    override val id: String
        get() = wrapped.id

    //Set by presenter (via repo) when Timer is paused
    private var elapsedTime: Int = 0
    private var paused: Boolean = false
}