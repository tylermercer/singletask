package com.tylermercer.timeboxer.tasks.runningtask

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.Toast
import com.tylermercer.timeboxer.R
import com.tylermercer.timeboxer.repositories.Injection
import com.tylermercer.timeboxer.tasks.runningtask.timerhelper.CombinedTimerHelper
import com.tylermercer.timeboxer.tasks.runningtask.timerhelper.TimerHelper
import kotlinx.android.synthetic.main.fragment_add_task.*
import kotlinx.android.synthetic.main.fragment_running_task.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RunningTaskFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RunningTaskFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

class RunningTaskFragment : Fragment(),
        RunningTaskContract.View,
        TimerHelper.OnTotalTimeElapsedListener {

    private lateinit var timerHelper: CombinedTimerHelper
    override fun initTimer(description: String, duration: Int, elapsed: Int, running: Boolean) {
        textview_running_task_description.text = description
        timerHelper = CombinedTimerHelper(
                duration,
                textview_countdown,
                progressbar,
                this
        )

        timerHelper.currentTimeElapsed = elapsed

        if (running) {
            updateButtons(false, true, true)
            timerHelper.start()
            presenter.onStartTimer()
        }

        fab_start.setOnClickListener({
            updateButtons(false, true, true)
            timerHelper.start()
            presenter.onStartTimer()
        })

        fab_pause.setOnClickListener({
            updateButtons(true, false, true)
            timerHelper.pause()
            presenter.onPauseTimer(timerHelper.currentTimeElapsed)
        })

        fab_stop.setOnClickListener({
            updateButtons(true, false, false)
            timerHelper.stop()
            presenter.onStopTimer()
        })
    }

    override fun onTotalTimeElapsed() {
        //TODO: Implement properly (i.e. just update the view)
        val v = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
        v?.vibrate(longArrayOf(0, 250, 750), 1)
        Toast.makeText(activity, "Total time elapsed!", Toast.LENGTH_SHORT).show()
    }

    private fun updateButtons(play: Boolean, pause: Boolean, stop: Boolean) {
        fab_start.isEnabled = play
        fab_pause.isEnabled = pause
        fab_stop.isEnabled = stop
    }

    private lateinit var presenter: RunningTaskPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter = RunningTaskPresenter(this, Injection.provideTaskRepository())
        return inflater.inflate(R.layout.fragment_running_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    private var listener: OnFragmentInteractionListener? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        timerHelper.pause() // No need to stop because it would result in calls to set the textview and progressbar
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onTaskCancelled()
        fun onTaskCompleted()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment with parameters ready to be loaded
         *
         * @return A new instance of fragment RunningTaskFragment.
         */
        @JvmStatic
        fun newInstance() = RunningTaskFragment()
    }
}
