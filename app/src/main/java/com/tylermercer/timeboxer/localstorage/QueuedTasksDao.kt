package com.tylermercer.timeboxer.localstorage

import com.tylermercer.timeboxer.repositories.entities.tasks.MutableTask

interface QueuedTasksDao {
    fun addTask(task: MutableTask)
    fun getQueuedTask(): MutableTask
    fun removeTask(id: String)
}