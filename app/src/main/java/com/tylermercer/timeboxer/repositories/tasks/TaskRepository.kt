package com.tylermercer.timeboxer.repositories.tasks

import com.tylermercer.timeboxer.repositories.entities.tasks.RunningTask
import com.tylermercer.timeboxer.repositories.entities.tasks.Task

/**
 * Acts as an aggregate root for the task entities (including tasks, tags, etc), and contains the
 * business logic pertaining to those entities.
 *
 * All properties are read-only, and all functions with non-primitive return values return read-only
 * interface types (such as [Task] or [RunningTask])
 *
 * @invariant: The current state of the repository is saved to the Database (if it exists)
 */
interface TaskRepository {

    interface OnRunningTaskRetrievedListener {
        fun onRunningTaskRetrieved(task: RunningTask)
    }

    fun addTask(description: String, durationSeconds: Int)
    fun getRunningTask(listener: TaskRepository.OnRunningTaskRetrievedListener)
}