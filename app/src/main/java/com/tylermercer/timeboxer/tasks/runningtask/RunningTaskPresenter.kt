package com.tylermercer.timeboxer.tasks.runningtask

import android.util.Log
import com.tylermercer.timeboxer.repositories.entities.tasks.RunningTask
import com.tylermercer.timeboxer.repositories.tasks.TaskRepository

class RunningTaskPresenter(
        private val view: RunningTaskContract.View,
        private val repo: TaskRepository
) : RunningTaskContract.Presenter {

    private lateinit var runningTask: RunningTask
    /**
     * Gets the task data from the repository. If the task is running, checks to see if an alarm exists, if not, creates an alarm
     */
    override fun onViewCreated() {
        repo.getRunningTask(object: TaskRepository.OnRunningTaskRetrievedListener {
            override fun onRunningTaskRetrieved(task: RunningTask) {
                runningTask = task
                view.initTimer(
                        runningTask.description,
                        runningTask.durationSeconds,
                        runningTask.elapsedTimeSeconds,
                        !runningTask.isPaused
                )
                //TODO: check if is running, if so, create an alarm if necessary
            }
        })
    }

    /**
     * Creates the alarms and marks the task as begun in the repo
     */
    override fun onStartTimer() {
        Log.d("RunningTaskPresenter", "onStartTimer called")
    }

    /**
     * Removes the alarms and marks the task as paused in the repo
     */
    override fun onPauseTimer(timeElapsed: Int) {
        Log.d("RunningTaskPresenter", "onPauseTimer called")
    }

    /**
     * Removes the alarms and resets the task in the repo
     */
    override fun onStopTimer() {
        Log.d("RunningTaskPresenter", "onStopTimer called")
    }
}