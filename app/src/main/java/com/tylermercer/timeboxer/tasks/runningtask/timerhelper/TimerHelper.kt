package com.tylermercer.timeboxer.tasks.runningtask.timerhelper

interface TimerHelper {
    var currentTimeRemaining: Int
    var currentTimeElapsed: Int
    val isRunning: Boolean

    fun start()
    fun stop()
    fun pause()

    interface OnTotalTimeElapsedListener {
        fun onTotalTimeElapsed()
    }
}