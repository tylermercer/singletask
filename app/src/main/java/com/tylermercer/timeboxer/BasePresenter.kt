package com.tylermercer.timeboxer

interface BasePresenter {
    fun onViewCreated()
}
