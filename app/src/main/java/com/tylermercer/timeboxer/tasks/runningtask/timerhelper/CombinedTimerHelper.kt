package com.tylermercer.timeboxer.tasks.runningtask.timerhelper

import android.widget.ProgressBar
import android.widget.TextView
import java.text.SimpleDateFormat

class CombinedTimerHelper(
        maxTimeSeconds: Int,
        textView: TextView,
        progressBar: ProgressBar,
        listener: TimerHelper.OnTotalTimeElapsedListener?
) : TimerHelper {

    override var currentTimeRemaining: Int
        get() = progressBarTimerHelper.currentTimeRemaining
        set(value) {
            progressBarTimerHelper.currentTimeRemaining = value
            textViewTimerHelper.currentTimeRemaining = value
        }
    override var currentTimeElapsed: Int
        get() = progressBarTimerHelper.currentTimeRemaining
        set(value) {
            progressBarTimerHelper.currentTimeElapsed = value
            textViewTimerHelper.currentTimeElapsed = value
        }
    override val isRunning: Boolean
        get() = progressBarTimerHelper.isRunning

    private var progressBarTimerHelper: ProgressBarTimerHelper
            = ProgressBarTimerHelper(maxTimeSeconds, progressBar, listener)

    private var textViewTimerHelper: TextViewTimerHelper
            = TextViewTimerHelper(maxTimeSeconds, textView, null) //Listener is null here to ensure only one callback

    override fun start() {
        progressBarTimerHelper.start()
        textViewTimerHelper.start()
    }

    override fun stop() {
        progressBarTimerHelper.stop()
        textViewTimerHelper.stop()
    }

    override fun pause() {
        progressBarTimerHelper.pause()
        textViewTimerHelper.pause()
    }
}