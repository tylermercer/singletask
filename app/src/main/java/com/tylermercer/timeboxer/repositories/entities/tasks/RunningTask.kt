package com.tylermercer.timeboxer.repositories.entities.tasks

interface RunningTask : Task {
    val elapsedTimeSeconds: Int
    val isPaused: Boolean
}