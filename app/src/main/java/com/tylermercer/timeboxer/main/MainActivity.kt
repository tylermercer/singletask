package com.tylermercer.timeboxer.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tylermercer.timeboxer.R
import com.tylermercer.timeboxer.tasks.addtask.AddTaskFragment
import com.tylermercer.timeboxer.tasks.runningtask.RunningTaskFragment

private const val FRAG_TAG_TASK = "com.tylermercer.timeboxer.main.MainActivity.TASK_TAG"

class MainActivity : AppCompatActivity(), AddTaskFragment.OnTaskAddListener, RunningTaskFragment.OnFragmentInteractionListener {

    override fun onTaskCompleted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTaskCancelled() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTaskAdded(description: String, durationSeconds: Int) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_frame, RunningTaskFragment.newInstance(), FRAG_TAG_TASK)
                .addToBackStack(null) //TODO: Remove this line
                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.main_frame, AddTaskFragment.newInstance(), FRAG_TAG_TASK)
                    .commit()
        }
    }
}
