package com.tylermercer.timeboxer.localstorage

import com.tylermercer.timeboxer.repositories.entities.tasks.MutableRunningTask

interface RunningTaskDao {
    //Running task
    fun getRunningTask(): MutableRunningTask?
    fun setRunningTask(task: MutableRunningTask)
    fun clearRunningTask()
}